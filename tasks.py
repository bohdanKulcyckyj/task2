import time
import sys
class Vector:
    def __init__(self, x = 0, y = 0, z = 0):
        self.x = x
        self.y = y
        self.z = z

    def __add__(self, other):
        if isinstance(other, Vector):
            return Vector(self.x + other.x, self.y + other.y, self.z + other.z)
        raise ValueError

    def __sub__(self, other):
        if isinstance(other, Vector):
            return Vector(self.x - other.x, self.y - other.y, self.z - other.z)
        raise ValueError

    def __eq__(self, other):
        if isinstance(other, Vector):
            if self.x != other.x or self.y != other.y or self.z != other.z:
                return False
            else:
                return True
        else:
            return False

    def __str__(self):
        return f"({self.x}, {self.y}, {self.z})"

    def __iter__(self):
        yield self.x
        yield self.y
        yield self.z

    def __getitem__(self, index):
        if index == 0:
            return self.x
        elif index == 1:
            return self.y
        elif index == 2:
            return self.z
        else:
            raise IndexError

    def __setitem__(self, index, value):
        if index == 0:
            self.x = value
        elif index == 1:
            self.y = value
        elif index == 2:
            self.z = value
        else:
            raise IndexError

class UpperCaseDecorator:
    def __init__(self, file):
        self.file = file

    def write(self, string):
        str = ''
        for c in string:
            if not c.isupper():
                str += c.upper()        
        self.file.write(str)

    def writelines(self, list):
        for str in list:
            self.write(str)
